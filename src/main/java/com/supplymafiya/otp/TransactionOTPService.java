package com.supplymafiya.otp;

import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.JsonNode;
import com.mashape.unirest.http.Unirest;
import com.mashape.unirest.http.exceptions.UnirestException;

public class TransactionOTPService implements IOTPService {
	
	MessageDTO messageDTO;
	
	public TransactionOTPService(MessageDTO messageDTO) {
		this.messageDTO=messageDTO;
	}

	public JsonNode sendOTPMessage(String mobileNumber) throws UnirestException {
		HttpResponse<JsonNode> jsonResponse = Unirest.post(messageDTO.getSentURL())
  			  .header("accept", "application/json")
  			  .queryString("otp_length", messageDTO.getOtpLength())
  			  .field("authkey",  messageDTO.getAuthKey())
  			  .field("message",  messageDTO.getMessage())
  			  .field("sender", messageDTO.getSender())
  			  .field("mobile", mobileNumber)
  			  .asJson();
		return jsonResponse.getBody();
	}

	public JsonNode resendOTPMessage(String mobileNumber) throws UnirestException {
		HttpResponse<JsonNode> jsonResponse = Unirest.post(messageDTO.getResentURL())
	  			  .header("accept", "application/json")
	  			  .queryString("authkey",  messageDTO.getAuthKey())
	  			  .field("retrytype", messageDTO.getRetrytype())
	  			  .field("mobile",mobileNumber)
	  			  .asJson();
		return jsonResponse.getBody();
	}
	

	public JsonNode verifyOTP(String mobileNumber, String otp) throws UnirestException {
		HttpResponse<JsonNode> jsonResponse = Unirest.post(messageDTO.getVerificationURL())
	  			  .header("accept", "application/json")
	  			  .queryString("authkey",  messageDTO.getAuthKey())
	  			  .field("mobile",mobileNumber)
	  			  .field("otp",  otp)
	  			  .asJson();
		return jsonResponse.getBody();
	}

}
