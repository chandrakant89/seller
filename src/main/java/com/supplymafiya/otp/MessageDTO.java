package com.supplymafiya.otp;

public class MessageDTO {
	private String otpLength;
	private String authKey;
	private String message;
	private String sender;
	private String mobileNumber;
	private String otpNumber;
	private String sentURL;
	private String resentURL;
	private String retrytype;
	
	private String verificationURL;
	public String getOtpLength() {
		return otpLength;
	}
	public void setOtpLength(String otpLength) {
		this.otpLength = otpLength;
	}
	public String getAuthKey() {
		return authKey;
	}
	public void setAuthKey(String authKey) {
		this.authKey = authKey;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public String getSender() {
		return sender;
	}
	public void setSender(String sender) {
		this.sender = sender;
	}
	public String getMobileNumber() {
		return mobileNumber;
	}
	public void setMobileNumber(String mobileNumber) {
		this.mobileNumber = mobileNumber;
	}
	public String getOtpNumber() {
		return otpNumber;
	}
	public void setOtpNumber(String otpNumber) {
		this.otpNumber = otpNumber;
	}
	public String getSentURL() {
		return sentURL;
	}
	public void setSentURL(String sentURL) {
		this.sentURL = sentURL;
	}
	public String getResentURL() {
		return resentURL;
	}
	public void setResentURL(String resentURL) {
		this.resentURL = resentURL;
	}
	public String getVerificationURL() {
		return verificationURL;
	}
	public void setVerificationURL(String verificationURL) {
		this.verificationURL = verificationURL;
	}
	public String getRetrytype() {
		return retrytype;
	}
	public void setRetrytype(String retrytype) {
		this.retrytype = retrytype;
	}
	
	
	

}
