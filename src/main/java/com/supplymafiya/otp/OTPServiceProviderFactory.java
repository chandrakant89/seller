package com.supplymafiya.otp;

public class OTPServiceProviderFactory {
	private static IOTPService smd91iotpService;
	public static  IOTPService getOTPService(String serviceName){
		if(serviceName.equalsIgnoreCase("SMS91")){
			if(smd91iotpService!=null){
				return smd91iotpService;
			}
			MessageDTO messageDTO=new MessageDTO();
			//messageDTO.setAuthKey("237760AveyDRZiM5b9cf6b9");
			messageDTO.setAuthKey("237550AxgVNErEH6z5b9b8a25");
			messageDTO.setMessage("Your Suppy Mafia One Time Password is  ##OTP## ");
			messageDTO.setOtpLength("4");
			messageDTO.setSender("OTPSMS");
			messageDTO.setRetrytype("text");
			messageDTO.setSentURL("http://control.msg91.com/api/sendotp.php");
			messageDTO.setResentURL("http://control.msg91.com/api/retryotp.php");
			messageDTO.setVerificationURL("https://control.msg91.com/api/verifyRequestOTP.php");
			smd91iotpService=new TransactionOTPService(messageDTO);
		}
		return smd91iotpService;
	}

}
