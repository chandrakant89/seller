package com.supplymafiya.otp;

import com.mashape.unirest.http.JsonNode;
import com.mashape.unirest.http.exceptions.UnirestException;

public interface IOTPService {
	public abstract JsonNode sendOTPMessage(String mobileNumber) throws UnirestException;
	public abstract JsonNode resendOTPMessage(String mobileNumber)throws UnirestException;
	public abstract JsonNode verifyOTP(String mobileNumber,String otp)throws UnirestException;
}
