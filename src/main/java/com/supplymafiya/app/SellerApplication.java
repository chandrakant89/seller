package com.supplymafiya.app;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.boot.web.support.SpringBootServletInitializer;
import org.springframework.context.annotation.ComponentScan;

import com.supplymafiya.model.FileStorageProperties;

@SpringBootApplication
@ComponentScan(basePackages = { "com.supplymafiya", "com.supplymafiya.api", "com.supplymafiya.trange", "com.supplymafiya.trange.api"})
@EntityScan(basePackages = {"com.supplymafiya.model", "com.supplymafiya.trange.model"})

@EnableConfigurationProperties({
    FileStorageProperties.class
})
public class SellerApplication extends SpringBootServletInitializer {

	public static void main(String[] args) {
		SpringApplication.run(SellerApplication.class, args);
	}
	
	@Override
    protected SpringApplicationBuilder configure(SpringApplicationBuilder builder) {
        return builder.sources(SellerApplication.class);
    }
}
