package com.supplymafiya.response;

import com.fasterxml.jackson.annotation.JsonProperty;

public class ResponseMessageDTO {
	
	
	@JsonProperty(value = "message")
	private String message;

	@JsonProperty(value = "type")
	private String type;
	
	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}
	
	
}
