package com.supplymafiya.response;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonProperty;

public class LoginResponseDTO {
	
	@JsonProperty(value = "message")
	private String message;
	
	
	/*for User Response*/
	
	@JsonProperty("user_id")
	private long userId;
	
	
	@JsonProperty("user_role")
	private long userRole;
	
	
	@JsonProperty("owner_name")
	private String ownerName;
	
	
	@JsonProperty("owner_email")
	private String ownerEmail;
	
	
	@JsonProperty("password")
	private String password;
	
	
	@JsonProperty("owner_mobile")
	private String ownerMobile;
	
	
	@JsonProperty("owner_whatsapp_no")
	private String ownerWhatsappNo;
	
	
	@JsonProperty("gender")
	private String gender;
	
	
	@JsonProperty("state")
	private String state;
	
	
	@JsonProperty("district")
	private String district;
	
	
	@JsonProperty("block")
	private String block;
	
	
	@JsonProperty("representative_name")
	private String representativeName;
	
	
	@JsonProperty("representative_contact_no")
	private String representativeContactNo;
	
	
	@JsonProperty("representative_email_id")
	private String representativeEmailId;
	
	
	@JsonProperty("aadhar_card")
	private String aadharCard;
	
	
	@JsonProperty("pan_card")
	private String panCard;
	
	
	@JsonProperty("bussines_name")
	private String bussinesName;
	
	
	@JsonProperty("gstn")
	private String gstn;
	
	
	@JsonProperty("account_no")
	private String accountNo;
	

	@JsonProperty("account_holder_name")
	private String accountHolderName;
	
	
	@JsonProperty("branch")
	private String branch;
	
	
	@JsonProperty("ifsc_code")
	private String ifscCode;
	
	
	@JsonProperty("date")
	private Date date = new Date();
	
	
	@JsonProperty("added_by")
	private long addedBy;
	
	@JsonProperty("verify_status")
	private String verifyStatus;

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public long getUserId() {
		return userId;
	}

	public void setUserId(long userId) {
		this.userId = userId;
	}

	public long getUserRole() {
		return userRole;
	}

	public void setUserRole(long userRole) {
		this.userRole = userRole;
	}

	public String getOwnerName() {
		return ownerName;
	}

	public void setOwnerName(String ownerName) {
		this.ownerName = ownerName;
	}

	public String getOwnerEmail() {
		return ownerEmail;
	}

	public void setOwnerEmail(String ownerEmail) {
		this.ownerEmail = ownerEmail;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getOwnerMobile() {
		return ownerMobile;
	}

	public void setOwnerMobile(String ownerMobile) {
		this.ownerMobile = ownerMobile;
	}

	public String getOwnerWhatsappNo() {
		return ownerWhatsappNo;
	}

	public void setOwnerWhatsappNo(String ownerWhatsappNo) {
		this.ownerWhatsappNo = ownerWhatsappNo;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getDistrict() {
		return district;
	}

	public void setDistrict(String district) {
		this.district = district;
	}

	public String getBlock() {
		return block;
	}

	public void setBlock(String block) {
		this.block = block;
	}

	public String getRepresentativeName() {
		return representativeName;
	}

	public void setRepresentativeName(String representativeName) {
		this.representativeName = representativeName;
	}

	public String getRepresentativeContactNo() {
		return representativeContactNo;
	}

	public void setRepresentativeContactNo(String representativeContactNo) {
		this.representativeContactNo = representativeContactNo;
	}

	public String getRepresentativeEmailId() {
		return representativeEmailId;
	}

	public void setRepresentativeEmailId(String representativeEmailId) {
		this.representativeEmailId = representativeEmailId;
	}

	public String getAadharCard() {
		return aadharCard;
	}

	public void setAadharCard(String aadharCard) {
		this.aadharCard = aadharCard;
	}

	public String getPanCard() {
		return panCard;
	}

	public void setPanCard(String panCard) {
		this.panCard = panCard;
	}

	public String getBussinesName() {
		return bussinesName;
	}

	public void setBussinesName(String bussinesName) {
		this.bussinesName = bussinesName;
	}

	public String getGstn() {
		return gstn;
	}

	public void setGstn(String gstn) {
		this.gstn = gstn;
	}

	public String getAccountNo() {
		return accountNo;
	}

	public void setAccountNo(String accountNo) {
		this.accountNo = accountNo;
	}

	public String getAccountHolderName() {
		return accountHolderName;
	}

	public void setAccountHolderName(String accountHolderName) {
		this.accountHolderName = accountHolderName;
	}

	public String getBranch() {
		return branch;
	}

	public void setBranch(String branch) {
		this.branch = branch;
	}

	public String getIfscCode() {
		return ifscCode;
	}

	public void setIfscCode(String ifscCode) {
		this.ifscCode = ifscCode;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public long getAddedBy() {
		return addedBy;
	}

	public void setAddedBy(long addedBy) {
		this.addedBy = addedBy;
	}

	public String getVerifyStatus() {
		return verifyStatus;
	}

	public void setVerifyStatus(String verifyStatus) {
		this.verifyStatus = verifyStatus;
	}


}
