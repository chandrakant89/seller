package com.supplymafiya.repository;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.transaction.Transactional;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Repository;

import com.supplymafiya.exception.NotFoundException;
import com.supplymafiya.model.Product;
import com.supplymafiya.utility.SKUGenerater;

@Repository
public class ProductRepository {
	
Logger logger = Logger.getLogger(ProductRepository.class);
	
	@PersistenceContext	
	private EntityManager entityManager;
	
	@Transactional
	public long addProduct(Product product, long USER_ID)throws Exception{

		String sku = SKUGenerater.generateSKU();

		String queryStr = "INSERT INTO productdetail(" + 
				"	product_id, product_name, sku, category, brand, quality, special_use, river_area, firm_name, site_location, measurment_unit, cost_for_us, cost_for_vender, market_price, transport_facility, vehicle_types, user_id, product_image, date)" + 
				"	VALUES (DEFAULT, '"+product.getProductName()+"', '"+sku+"', '"+product.getCategory()+"', '"+product.getBrand()+"', '"+product.getQuality()+"', '"+product.getSpecialUse()+"', '"+product.getRiverArea()+"', '"+product.getFirmName()+"', '"+product.getSiteLocation()+"','"+product.getMeasurmentUnit()+"', '"+product.getCostForUs()+"', '"+product.getCostForVender()+"', '"+product.getMarketPrice()+"', '"+product.getTransportFacility()+"', '"+product.getVehicleTypes()+"', '"+USER_ID+"', '"+product.getProductImage()+"', '"+product.getDate()+"' )";

		logger.info("attribute_add_query::" + queryStr);
		Query query = entityManager.createNativeQuery(queryStr);
		long result =  query.executeUpdate();
		logger.debug("attribute_add_response::" + result);
		return result;
	}
	
	@Transactional
	public List<Product> getProductList(long USER_ID) {
		String queryStr = "select * from productdetail where user_id='"+USER_ID+"'";

		logger.info("attribute_add_query::" + queryStr);
		Query query = entityManager.createNativeQuery(queryStr, Product.class);
		@SuppressWarnings("unchecked")
		List<Product> result = (List<Product>) query.getResultList();
		logger.debug("attribute_add_response::" + result);
		if (result == null || result.size() == 0) {
			throw new NotFoundException("No results found");
		}
		return result;
		
	
	}

	/*public Product getProductById(long productId) {
		String queryStr = "select * from productdetail where product_id = '"+productId+"'";

		logger.info("attribute_add_query::" + queryStr);
		Query query = entityManager.createNativeQuery(queryStr);
		@SuppressWarnings("unchecked")
		Product result = (Product) query.getSingleResult();
		logger.debug("attribute_add_response::" + result);
		if (result == null) {
			throw new NotFoundException("No results found");
		}
		return result;
	}*/
	@Transactional
	public Product getProductById(long productId) {
		return entityManager.find(Product.class, productId);
		
	}

}
