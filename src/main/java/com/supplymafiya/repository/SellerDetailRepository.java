package com.supplymafiya.repository;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.transaction.Transactional;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Repository;

import com.supplymafiya.exception.NotFoundException;
import com.supplymafiya.model.Roles;
import com.supplymafiya.model.SellerDetail;
import com.supplymafiya.utility.EncryptDecriptPassword;

@Repository
public class SellerDetailRepository {

	Logger logger = Logger.getLogger(ProductRepository.class);

	@PersistenceContext
	private EntityManager entityManager;
	
	

	@Transactional
	public long addSeller(SellerDetail sellerDetail) throws Exception {
		
		//Roles roleid = sellerDetail.setUserRole(getRoles(10002));
		long roleid = 10002;
		String plainPassword = sellerDetail.getPassword();
		 int offset = 5;
		String encriptedPass = EncryptDecriptPassword.encrypt(plainPassword, offset);
		logger.info("Encripted Password: "+encriptedPass);
	
		String queryStr = "INSERT INTO sellerdetail("
				+ "	user_id, user_role, owner_name, owner_email, password, owner_mobile, owner_whatsapp_no, gender, state, district, block, representative_name, representative_contact_no, representative_email_id, aadhar_card, pan_card, bussines_name, gstn, account_no, account_holder_name, branch, ifsc_code, date, added_by, verify_status)"
				+ "	VALUES (DEFAULT, '" + roleid + "', '" + sellerDetail.getOwnerName() + "', '"
				+ sellerDetail.getOwnerEmail() + "', '" + encriptedPass + "', '" + sellerDetail.getOwnerMobile() + "', '" + sellerDetail.getOwnerWhatsappNo() + "', '" + sellerDetail.getGender()
				+ "', '" + sellerDetail.getState() + "', '" + sellerDetail.getDistrict() + "', '"
				+ sellerDetail.getBlock() + "', '" + sellerDetail.getRepresentativeName() + "','" + sellerDetail.getRepresentativeContactNo() + "', '" + sellerDetail.getRepresentativeEmailId() + "', '" + sellerDetail.getAadharCard() + "', '" + sellerDetail.getPanCard()
				+ "', '" + sellerDetail.getBussinesName() + "', '" + sellerDetail.getGstn() + "', '"
				+ sellerDetail.getAccountNo() + "', '" + sellerDetail.getAccountHolderName() + "', '"
				+ sellerDetail.getBranch() + "', '" + sellerDetail.getIfscCode() + "', '" + sellerDetail.getDate()
				+ "', '" + sellerDetail.getAddedBy() + "', '" + sellerDetail.getVerifyStatus() + "')";

		logger.info("attribute_add_query::" + queryStr);
		Query query = entityManager.createNativeQuery(queryStr);	
		@SuppressWarnings("unchecked")
		long result = query.executeUpdate();
		logger.debug("attribute_add_response::" + result);
		return result;
	}

	
	   public Roles getRoles(long roleId) { 
		// String queryStr = "select * from roles where role_id = '"+roleId+"'";
	   String queryStr = "from Roles where roleId = '"+roleId+"'";
	   logger.info("attribute_add_query::" + queryStr); 
	   Query query = entityManager.createQuery(queryStr, Roles.class); 
	   Roles result = (Roles) query.getSingleResult();
		logger.debug("sellerDetail::" + result);
		if (result == null) {
			throw new NotFoundException("No results found");
		}
	   return result;
	   
	   }
	 

	public SellerDetail getUserById(long userId) {
		
		String queryStr = "select * from sellerdetail where user_id = '"+userId+"'";
		logger.debug("sellerDetail::" + queryStr);
		Query query = entityManager.createNativeQuery(queryStr, SellerDetail.class);
		SellerDetail result = (SellerDetail) query.getSingleResult();
		logger.debug("sellerDetail::" + result);
		if (result == null) {
			throw new NotFoundException("No results found");
		}
		return result;
		 
	}

	public SellerDetail getUserByOwnerMobileNo(String ownerMobile){
		String queryStr = "select * from sellerdetail where owner_mobile = '"+ownerMobile+"'";
		logger.debug("sellerDetail::" + queryStr);
		Query query = entityManager.createNativeQuery(queryStr, SellerDetail.class);
		SellerDetail result = (SellerDetail) query.getSingleResult();
		logger.debug("sellerDetail::" + result);
		if (result == null) {
			throw new NotFoundException("No results found");
		}
		return result;
		
	} 


	public List<SellerDetail> getUserList(long addedBy) {
		String queryStr = "select * from sellerdetail where added_by='"+addedBy+"'";

		logger.info("attribute_add_query::" + queryStr);
		Query query = entityManager.createNativeQuery(queryStr, SellerDetail.class);
		@SuppressWarnings("unchecked")
		List<SellerDetail> result = (List<SellerDetail>) query.getResultList();
		logger.debug("attribute_add_response::" + result);
		if (result == null || result.size() == 0) {
			throw new NotFoundException("No results found");
		}
		return result;
	}


	@SuppressWarnings("null")
	public boolean isExits(String ownerMobile, String ownerEmail) {
		String queryStr = "select * from sellerdetail where owner_mobile = '"+ownerMobile+"' and owner_email = '"+ownerEmail+"'";
		logger.debug("sellerDetail::" + queryStr);
		Query query = entityManager.createNativeQuery(queryStr, SellerDetail.class);
		@SuppressWarnings("unchecked")
		List<SellerDetail> result = (List<SellerDetail>) query.getResultList();
		if (result != null || result.size()>0){
			return true;
		}
		return false;
		
	}

	@Transactional
	public long addNewPassword(String pass, long userId) {
		
		String plainPassword = pass;
		logger.info("Repository New Password: "+plainPassword);
		 int offset = 5;
		String encriptedPass = EncryptDecriptPassword.encrypt(plainPassword, offset);
		logger.info("Encripted Password: "+encriptedPass);
	
		String queryStr = "UPDATE sellerdetail SET password = '" + encriptedPass + "' WHERE user_id = '" + userId + "'";
		logger.info("attribute_add_query::" + queryStr);
		Query query = entityManager.createNativeQuery(queryStr);	
		@SuppressWarnings("unchecked")
		long result = query.executeUpdate();
		logger.debug("attribute_add_response::" + result);
		return result;
	}


	public SellerDetail getUserPassword(String mobile, long userId) {
		String queryStr = "select * from sellerdetail where owner_mobile = '"+mobile+"' and user_id = '"+userId+"'";
		logger.debug("sellerDetail::" + queryStr);
		Query query = entityManager.createNativeQuery(queryStr, SellerDetail.class);
		SellerDetail result = (SellerDetail) query.getSingleResult();
		logger.debug("sellerDetail::" + result);
		if (result == null) {
			throw new NotFoundException("No results found");
			
		}
		return result;
	}

	@Transactional
	public long addforgotPassword(String pass, String mobileNo) {
		String plainPassword = pass;
		logger.info("Repository New Password: "+plainPassword);
		 int offset = 5;
		String encriptedPass = EncryptDecriptPassword.encrypt(plainPassword, offset);
		logger.info("Encripted Password: "+encriptedPass);
	
		String queryStr = "UPDATE sellerdetail SET password = '" + encriptedPass + "' WHERE owner_mobile = '"+mobileNo+"'";
		logger.info("attribute_add_query::" + queryStr);
		Query query = entityManager.createNativeQuery(queryStr);	
		@SuppressWarnings("unchecked")
		long result = query.executeUpdate();
		logger.debug("attribute_add_response::" + result);
		return result;
	}

}
