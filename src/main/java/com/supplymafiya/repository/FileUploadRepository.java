package com.supplymafiya.repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.transaction.Transactional;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Repository;

import com.supplymafiya.model.ImageDetail;


@Repository
public class FileUploadRepository {
	
Logger logger = Logger.getLogger(FileUploadRepository.class);
	
	@PersistenceContext	
	private EntityManager entityManager;
	
	@Transactional
	public long addImageDetail(String url) {
		long userId= 10002;
		String alt = "brick";
		String metaData = "brick";
		long productId = 1234;
		String queryStr = "INSERT INTO image(" + 
				"image_id, product_image, alt, meta_data, product_id, user_id)" + 
				"VALUES (DEFAULT, '"+url+"', '"+alt+"', '"+metaData+"', '"+productId+"', '"+userId+"')";

		logger.info("attribute_add_query::" + queryStr);
		Query query = entityManager.createNativeQuery(queryStr, ImageDetail.class);
		@SuppressWarnings("unchecked")
		long result =  query.executeUpdate();
		logger.debug("attribute_add_response::" + result);
		return result;
	}
	
}

