package com.supplymafiya.repository;

import java.sql.Connection;
import java.sql.SQLException;

import javax.sql.DataSource;

public class MultiConnection {

	static DataSource dataSource;
	
	public static Connection getConnection() throws SQLException {
		return dataSource.getConnection();
	}
}
