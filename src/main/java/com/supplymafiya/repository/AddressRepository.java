package com.supplymafiya.repository;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.transaction.Transactional;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Repository;

import com.supplymafiya.exception.NotFoundException;
import com.supplymafiya.model.Block;
import com.supplymafiya.model.District;
import com.supplymafiya.model.State;

@Repository
public class AddressRepository {
	Logger logger = Logger.getLogger(AddressRepository.class);
	
	@PersistenceContext	
	private EntityManager entityManager;
	
	@Transactional
	public List<State> get()throws Exception{
		String queryStr = "select * from state";

		logger.info("attribute_add_query::" + queryStr);
		Query query = entityManager.createNativeQuery(queryStr, State.class);
		@SuppressWarnings("unchecked")
		List<State> result = (List<State>) query.getResultList();
		logger.debug("attribute_add_response::" + result);
		if (result == null || result.size() == 0) {
			throw new NotFoundException("No results found");
		}
		return result;
	}
	
	@Transactional
	public List<District> getDistrictList(long stateId)throws Exception{
		String queryStr = "select * from district where state_id = '"+stateId+"'";

		logger.info("attribute_add_query::" + queryStr);
		Query query = entityManager.createNativeQuery(queryStr, District.class);
		@SuppressWarnings("unchecked")
		List<District> result = (List<District>) query.getResultList();
		logger.debug("attribute_add_response::" + result);
		return result;
	}
	
	@Transactional
	public List<Block> getBlockList(long districtId)throws Exception{
		String queryStr = "select * from block where district_id = '"+districtId+"'";

		logger.info("attribute_add_query::" + queryStr);
		Query query = entityManager.createNativeQuery(queryStr, Block.class);
		@SuppressWarnings("unchecked")
		List<Block> result = (List<Block>) query.getResultList();
		logger.debug("attribute_add_response::" + result);
		return result;
	}
	
}
