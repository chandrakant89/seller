package com.supplymafiya.model;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.TypeDef;
import org.springframework.validation.annotation.Validated;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.supplymafiya.config.MyJsonType;

import io.swagger.annotations.ApiModel;
@ApiModel(description = "Assignment attributes.")
@Validated
@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2018-03-23T06:44:57.038+05:30")
@TypeDef(name = "MyJsonType", typeClass = MyJsonType.class)
@Entity
@Table(name="image")
public class ImageDetail {
	
	@Id
	@JsonProperty("image_id")
	private long imageId;
	
	@JsonProperty("product_image")
	private String productImage;
	
	@JsonProperty("alt")
	private String alt = null;
	
	@JsonProperty("meta_data")
	private String metaData = null;
	
	@JsonProperty("product_id")
	private long productId;
	
	@JsonProperty("user_id")
	private long userId;
	
	/*@JsonProperty("image_file")
	private File imageFile;
	*/
	
	public ImageDetail() {
		super();
	}

	public ImageDetail(long imageId, String productImage, String alt, String metaData, long productId, long userId) {
		super();
		this.imageId = imageId;
		this.productImage = productImage;
		this.alt = alt;
		this.metaData = metaData;
		this.productId = productId;
		this.userId = userId;
	}

	public long getImageId() {
		return imageId;
	}

	public void setImageId(long imageId) {
		this.imageId = imageId;
	}

	public String getProductImage() {
		return productImage;
	}

	public void setProductImage(String productImage) {
		this.productImage = productImage;
	}

	public String getAlt() {
		return alt;
	}

	public void setAlt(String alt) {
		this.alt = alt;
	}

	public String getMetaData() {
		return metaData;
	}

	public void setMetaData(String metaData) {
		this.metaData = metaData;
	}

	public long getProductId() {
		return productId;
	}

	public void setProductId(long productId) {
		this.productId = productId;
	}

	public long getUserId() {
		return userId;
	}

	public void setUserId(long userId) {
		this.userId = userId;
	}

	/*public File getImageFile() {
		return imageFile;
	}

	public void setImageFile(File imageFile) {
		this.imageFile = imageFile;
	}*/
	
	

}
