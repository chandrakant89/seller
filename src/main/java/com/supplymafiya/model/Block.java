package com.supplymafiya.model;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.TypeDef;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.supplymafiya.config.MyJsonType;

import io.swagger.annotations.ApiModel;

@ApiModel(description = "Assignment attributes.")
@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2018-03-23T06:44:57.038+05:30")
@Table(name = "block")
@Entity
@TypeDef(name = "MyJsonType", typeClass = MyJsonType.class)
public class Block {
	
	@Id
	@JsonProperty("block_id")
	private Long blockId;
	
	@JsonProperty("block_name")
	private String blockName;
	
	@JsonProperty("district_id")
	private long districtId;
	
	public Block() {
		super();
	}

	public Block(long blockId, String blockName, long districtId) {
		super();
		this.blockId = blockId;
		this.blockName = blockName;
		this.districtId = districtId;
	}

	public long getBlockId() {
		return blockId;
	}

	public void setBlockId(long blockId) {
		this.blockId = blockId;
	}

	public String getBlockName() {
		return blockName;
	}

	public void setBlockName(String blockName) {
		this.blockName = blockName;
	}

	public long getDistrictId() {
		return districtId;
	}

	public void setDistrictId(long districtId) {
		this.districtId = districtId;
	}
	
	

}
