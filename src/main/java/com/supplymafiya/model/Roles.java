package com.supplymafiya.model;

import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.TypeDef;
import org.springframework.validation.annotation.Validated;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.supplymafiya.config.MyJsonType;

import io.swagger.annotations.ApiModel;

@ApiModel(description = "Seller Group attributes.")
@Validated
@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2018-02-26T10:47:25.270+05:30")
@Table(name="roles")
@Entity
@TypeDef(name = "MyJsonType", typeClass = MyJsonType.class)
public class Roles {
	
	@Id
	@JsonProperty("role_id")
	private long roleId;
	
	@JsonProperty("role")
	private String role;
	

	public Roles() {
		super();
	}

	public Roles(long roleId, String role) {
		super();
		this.roleId = roleId;
		this.role = role;
	}

	public long getRoleId() {
		return roleId;
	}

	public void setRoleId(long roleId) {
		this.roleId = roleId;
	}

	public String getRole() {
		return role;
	}

	public void setRole(String role) {
		this.role = role;
	}
	
	

}
