package com.supplymafiya.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.TypeDef;
import org.springframework.validation.annotation.Validated;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.supplymafiya.config.MyJsonType;

import io.swagger.annotations.ApiModel;

@ApiModel(description = "Assignment attributes.")
@Validated
@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2018-03-23T06:44:57.038+05:30")
@TypeDef(name = "MyJsonType", typeClass = MyJsonType.class)
@Entity
@Table(name="productdetail")
public class Product {
	
	@Id
	@JsonProperty("product_id")
	private long productId = 0;
	
	@Column(name="product_name")
	@JsonProperty("product_name")
	private String productName = null;
	
	@Column(name="sku")
	@JsonProperty("sku")
	private String sku = null;
	
	@Column(name="category")
	@JsonProperty("category")
	private String category = null;
	
	@Column(name="brand")
	@JsonProperty("brand")
	private String brand = null;
	
	@Column(name="quality")
	@JsonProperty("quality")
	private String quality;
	
	@Column(name="special_use")
	@JsonProperty("special_use")
	private String specialUse = null;
	
	@Column(name="river_area")
	@JsonProperty("river_area")
	private String riverArea = null;
	
	@Column(name="firm_name")
	@JsonProperty("firm_name")
	private String firmName = null;
	
	@Column(name="site_location")
	@JsonProperty("site_location")
	private String siteLocation = null;
	
	@Column(name="measurment_unit")
	@JsonProperty("measurment_unit")
	private String measurmentUnit = null;
	
	@Column(name="cost_for_us")
	@JsonProperty("cost_for_us")
	private double costForUs = 0;
	
	@Column(name="cost_for_vender")
	@JsonProperty("cost_for_vender")
	private double costForVender = 0;
	
	@Column(name="market_price")
	@JsonProperty("market_price")
	private double marketPrice = 0;
	
	@Column(name="transport_facility")
	@JsonProperty("transport_facility")
	private String transportFacility = null;
	
	@Column(name="vehicle_types")
	@JsonProperty("vehicle_types")
	private String vehicleTypes = null;
	
	@Column(name="user_id")
	@JsonProperty("user_id")
	private long userId = 0;
	
	@Column(name="product_image")
	@JsonProperty("product_image")
	private String productImage = null;
	
	private Date date = new Date();
	
	public Product() {
		
	}

	public Product(long productId, String productName, String sku, String category, String brand, String quality,
			String specialUse, String riverArea, String firmName, String siteLocation, String measurmentUnit,
			double costForUs, double costForVender, double marketPrice, String transportFacility, String vehicleTypes,
			long userId, String productImage, Date date) {
		super();
		this.productId = productId;
		this.productName = productName;
		this.sku = sku;
		this.category = category;
		this.brand = brand;
		this.quality = quality;
		this.specialUse = specialUse;
		this.riverArea = riverArea;
		this.firmName = firmName;
		this.siteLocation = siteLocation;
		this.measurmentUnit = measurmentUnit;
		this.costForUs = costForUs;
		this.costForVender = costForVender;
		this.marketPrice = marketPrice;
		this.transportFacility = transportFacility;
		this.vehicleTypes = vehicleTypes;
		this.userId = userId;
		this.productImage = productImage;
		this.date = date;
	}

	public long getProductId() {
		return productId;
	}

	public void setProductId(long productId) {
		this.productId = productId;
	}

	public String getProductName() {
		return productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	public String getSku() {
		return sku;
	}

	public void setSku(String sku) {
		this.sku = sku;
	}

	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	public String getBrand() {
		return brand;
	}

	public void setBrand(String brand) {
		this.brand = brand;
	}

	public String getQuality() {
		return quality;
	}

	public void setQuality(String quality) {
		this.quality = quality;
	}

	public String getSpecialUse() {
		return specialUse;
	}

	public void setSpecialUse(String specialUse) {
		this.specialUse = specialUse;
	}

	public String getRiverArea() {
		return riverArea;
	}

	public void setRiverArea(String riverArea) {
		this.riverArea = riverArea;
	}

	public String getFirmName() {
		return firmName;
	}

	public void setFirmName(String firmName) {
		this.firmName = firmName;
	}

	public String getSiteLocation() {
		return siteLocation;
	}

	public void setSiteLocation(String siteLocation) {
		this.siteLocation = siteLocation;
	}

	public String getMeasurmentUnit() {
		return measurmentUnit;
	}

	public void setMeasurmentUnit(String measurmentUnit) {
		this.measurmentUnit = measurmentUnit;
	}

	public double getCostForUs() {
		return costForUs;
	}

	public void setCostForUs(double costForUs) {
		this.costForUs = costForUs;
	}

	public double getCostForVender() {
		return costForVender;
	}

	public void setCostForVender(double costForVender) {
		this.costForVender = costForVender;
	}

	public double getMarketPrice() {
		return marketPrice;
	}

	public void setMarketPrice(double marketPrice) {
		this.marketPrice = marketPrice;
	}

	public String getTransportFacility() {
		return transportFacility;
	}

	public void setTransportFacility(String transportFacility) {
		this.transportFacility = transportFacility;
	}

	public String getVehicleTypes() {
		return vehicleTypes;
	}

	public void setVehicleTypes(String vehicleTypes) {
		this.vehicleTypes = vehicleTypes;
	}

	public long getUserId() {
		return userId;
	}

	public void setUserId(long userId) {
		this.userId = userId;
	}

	public String getProductImage() {
		return productImage;
	}

	public void setProductImage(String productImage) {
		this.productImage = productImage;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}
	
	
	
}
