package com.supplymafiya.model;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.TypeDef;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.supplymafiya.config.MyJsonType;

import io.swagger.annotations.ApiModel;

@ApiModel(description = "Assignment attributes.")
@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2018-03-23T06:44:57.038+05:30")
@Table(name = "district")
@Entity
@TypeDef(name = "MyJsonType", typeClass = MyJsonType.class)
public class District {
	@Id
	@JsonProperty("district_id")
	private long districtId;
	
	@JsonProperty("district_name")
	private String districtName;
	
	@JsonProperty("state_id")
	private long stateId;
	
	public District() {
		super();
	}

	public District(long districtId, String districtName, long stateId) {
		super();
		this.districtId = districtId;
		this.districtName = districtName;
		this.stateId = stateId;
	}

	public long getDistrictId() {
		return districtId;
	}

	public void setDistrictId(long districtId) {
		this.districtId = districtId;
	}

	public String getDistrictName() {
		return districtName;
	}

	public void setDistrictName(String districtName) {
		this.districtName = districtName;
	}

	public long getStateId() {
		return stateId;
	}

	public void setStateId(long stateId) {
		this.stateId = stateId;
	}
	
	

}
