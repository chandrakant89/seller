package com.supplymafiya.model;

public class LoginRequest {

	
	private String mobile;
	
	private String password;
	
	private String emilid;
	
	private Boolean rememberme;
	
	
	public LoginRequest() {
		super();
	}
	
	
	public LoginRequest(String mobile, String password, String emilid, Boolean rememberme) {
		super();
		this.mobile = mobile;
		this.password = password;
		this.emilid = emilid;
		this.rememberme = rememberme;
	}


	public String getMobile() {
		return mobile;
	}
	public void setMobile(String mobile) {
		this.mobile = mobile;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getEmilid() {
		return emilid;
	}
	public void setEmilid(String emilid) {
		this.emilid = emilid;
	}


	public Boolean getRememberme() {
		return rememberme;
	}


	public void setRememberme(Boolean rememberme) {
		this.rememberme = rememberme;
	}
	
	
	
}
