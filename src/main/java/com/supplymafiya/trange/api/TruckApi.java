package com.supplymafiya.trange.api;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.supplymafiya.model.ErrorResponse;
import com.supplymafiya.model.SellerDetail;
import com.supplymafiya.response.ResponseMessageDTO;
import com.supplymafiya.trange.model.Truck;
import com.supplymafiya.trange.service.TruckService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2018-02-28T09:43:00.518+05:30")

@RestController
@RequestMapping("/seller")
@CrossOrigin(maxAge = 3600, allowedHeaders = { "Content-Type", "Accept"}, allowCredentials = "false", methods = { RequestMethod.GET,
				RequestMethod.POST, RequestMethod.PATCH, RequestMethod.DELETE }, origins = { "*" })
@Api(value = "add truck")
public class TruckApi {
	
	@Autowired
	TruckService truckService;
	
	@ApiOperation(value = "add Truck", tags = "truck")
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Success"),
			@ApiResponse(code = 400, message = "Bad request", response = ErrorResponse.class),
			@ApiResponse(code = 401, message = "Unauthorized", response = ErrorResponse.class),
			@ApiResponse(code = 404, message = "Not found", response = ErrorResponse.class),
			@ApiResponse(code = 405, message = "Validation exception", response = ErrorResponse.class) })
	@PostMapping(value = "/addtruck", produces = { "application/json" }, consumes = { "application/json" })
	public ResponseEntity<ResponseMessageDTO> addTruck(
			@RequestBody Truck truck) throws Exception {
		long data = truckService.addTruck(truck);
		ResponseMessageDTO response = new ResponseMessageDTO();
		response.setMessage("truck Registered Successfully");
		return new ResponseEntity<ResponseMessageDTO>(response, HttpStatus.CREATED);
	}
	
	@ApiOperation(value = "get Truck list", tags = "truck", response = Truck.class, responseContainer = "List")
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Success", response = Truck.class),
			@ApiResponse(code = 400, message = "Bad request", response = ErrorResponse.class),
			@ApiResponse(code = 401, message = "Unauthorized", response = ErrorResponse.class),
			@ApiResponse(code = 404, message = "Not found", response = ErrorResponse.class),
			@ApiResponse(code = 405, message = "Validation exception", response = ErrorResponse.class) })
	@RequestMapping(value = "/gettrucklist/{user_id}", produces = { "application/json" }, method = RequestMethod.GET)
	public ResponseEntity<List<Truck>> getTruckList(
			@ApiParam(value = "User ID" ,required=true) @PathVariable("user_id") long userId) throws Exception {
		List<Truck> response = truckService.getTruckList(userId);
		return new ResponseEntity<List<Truck>>(response, HttpStatus.OK);
	}
	
	
	@ApiOperation(value = "get All Truck list", tags = "truck", response = Truck.class, responseContainer = "List")
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Success", response = Truck.class),
			@ApiResponse(code = 400, message = "Bad request", response = ErrorResponse.class),
			@ApiResponse(code = 401, message = "Unauthorized", response = ErrorResponse.class),
			@ApiResponse(code = 404, message = "Not found", response = ErrorResponse.class),
			@ApiResponse(code = 405, message = "Validation exception", response = ErrorResponse.class) })
	@RequestMapping(value = "/getalltrucklist", produces = { "application/json" }, method = RequestMethod.GET)
	public ResponseEntity<List<Truck>> getAllTruckList() throws Exception {
		List<Truck> response = truckService.getAllTruckList();
		return new ResponseEntity<List<Truck>>(response, HttpStatus.OK);
	}

}
