package com.supplymafiya.trange.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.supplymafiya.trange.Repository.TruckRepository;
import com.supplymafiya.trange.model.Truck;

@Service
public class TruckService {
	
	@Autowired
	TruckRepository truckRepository;

	public long addTruck(Truck truck) {
		long data = truckRepository.addTruck(truck);
		return data;
		
	}
	
	public List<Truck> getTruckList(long userId) {
		List<Truck> truckList = truckRepository.getTruckList(userId);
		return truckList;
	}
	
	public List<Truck> getAllTruckList() {
		List<Truck> truckList = truckRepository.getAllTruckList();
		return truckList;
	}

}
