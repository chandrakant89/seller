package com.supplymafiya.trange.Repository;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.transaction.Transactional;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Repository;

import com.supplymafiya.exception.NotFoundException;
import com.supplymafiya.trange.model.Truck;

@Repository
public class TruckRepository {
	
	Logger logger = Logger.getLogger(TruckRepository.class);
	
	@PersistenceContext
	private EntityManager entityManager;
	
	@Transactional
	public long addTruck(Truck truck) {
		String queryStr = "INSERT INTO truck("
				+ "	truck_id, truck_no, model_no, fuel_avg, insurance_no, driver_name, driver_contact_no, driver_id, drivery_license_no, max_transport_capacity, material, user_id)"
				+ "	VALUES (DEFAULT, '" + truck.getTruckNo() + "', '" + truck.getModelNo() + "', '"
				+ truck.getFuelAvg() + "', '" + truck.getInsuranceNo() + "', '" + truck.getDriverName() + "', '" + truck.getDriverContactNo() + "', '" + truck.getDriverId()
				+ "', '" + truck.getDriveryLicenseNo() + "', '" + truck.getMaxTransportCapacity() + "', '"
				+ truck.getMaterial() + "', '" + truck.getUserId() + "')";

		logger.info("attribute_add_query::" + queryStr);
		Query query = entityManager.createNativeQuery(queryStr);	
		@SuppressWarnings("unchecked")
		long result = query.executeUpdate();
		logger.debug("attribute_add_response::" + result);
		return result;
	
	}
	
	
	public List<Truck> getTruckList(long userId) {
		String queryStr = "select * from truck where user_id = '"+userId+"'";

		logger.info("attribute_add_query::" + queryStr);
		Query query = entityManager.createNativeQuery(queryStr, Truck.class);
		@SuppressWarnings("unchecked")
		List<Truck> result = (List<Truck>) query.getResultList();
		logger.debug("attribute_add_response::" + result);
		if (result == null || result.size() == 0) {
			throw new NotFoundException("No results found");
		}
		return result;
	}
	
	public List<Truck> getAllTruckList() {
		String queryStr = "select * from truck" ;

		logger.info("attribute_add_query::" + queryStr);
		Query query = entityManager.createNativeQuery(queryStr, Truck.class);
		@SuppressWarnings("unchecked")
		List<Truck> result = (List<Truck>) query.getResultList();
		logger.debug("attribute_add_response::" + result);
		if (result == null || result.size() == 0) {
			throw new NotFoundException("No results found");
		}
		return result;
	}
	

}
