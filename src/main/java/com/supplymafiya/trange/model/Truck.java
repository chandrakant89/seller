package com.supplymafiya.trange.model;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.TypeDef;
import org.springframework.validation.annotation.Validated;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.supplymafiya.config.MyJsonType;

import io.swagger.annotations.ApiModel;

@ApiModel(description = "Truck Group attributes.")
@Validated
@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2018-02-26T10:47:25.270+05:30")
@Table(name="truck")
@Entity
@TypeDef(name = "MyJsonType", typeClass = MyJsonType.class)
public class Truck {
	
	@Id
	@JsonProperty("truck_id")
	private long truckId;
	
	@JsonProperty("truck_no")
	private String truckNo;
	
	@JsonProperty("model_no")
	private String modelNo;
	
	@JsonProperty("fuel_avg")
	private String fuelAvg;
	
	@JsonProperty("insurance_no")
	private String insuranceNo;
	
	@JsonProperty("driver_name")
	private String driverName;
	
	@JsonProperty("driver_contact_no")
	private String driverContactNo;
	
	@JsonProperty("driver_id")
	private String driverId;
	
	@JsonProperty("drivery_license_no")
	private String driveryLicenseNo;
	
	@JsonProperty("max_transport_capacity")
	private String maxTransportCapacity;
	
	@JsonProperty("material")
	private String material;
	
	@JsonProperty("user_id")
	private long userId;
	
	public Truck() {
		super();
	}

	public Truck(long truckId, String truckNo, String modelNo, String fuelAvg, String insuranceNo, String driverName,
			String driverContactNo, String driverId, String driveryLicenseNo, String maxTransportCapacity, String material, long userId) {
		super();
		this.truckId = truckId;
		this.truckNo = truckNo;
		this.modelNo = modelNo;
		this.fuelAvg = fuelAvg;
		this.insuranceNo = insuranceNo;
		this.driverName = driverName;
		this.driverContactNo = driverContactNo;
		this.driverId = driverId;
		this.driveryLicenseNo = driveryLicenseNo;
		this.maxTransportCapacity = maxTransportCapacity;
		this.material = material;
		this.userId = userId;
	}

	public long getTruckId() {
		return truckId;
	}

	public void setTruckId(long truckId) {
		this.truckId = truckId;
	}

	public String getTruckNo() {
		return truckNo;
	}

	public void setTruckNo(String truckNo) {
		this.truckNo = truckNo;
	}

	public String getModelNo() {
		return modelNo;
	}

	public void setModelNo(String modelNo) {
		this.modelNo = modelNo;
	}

	public String getFuelAvg() {
		return fuelAvg;
	}

	public void setFuelAvg(String fuelAvg) {
		this.fuelAvg = fuelAvg;
	}

	public String getInsuranceNo() {
		return insuranceNo;
	}

	public void setInsuranceNo(String insuranceNo) {
		this.insuranceNo = insuranceNo;
	}

	public String getDriverName() {
		return driverName;
	}

	public void setDriverName(String driverName) {
		this.driverName = driverName;
	}


	public String getDriverContactNo() {
		return driverContactNo;
	}

	public void setDriverContactNo(String driverContactNo) {
		this.driverContactNo = driverContactNo;
	}

	public String getDriverId() {
		return driverId;
	}

	public void setDriverId(String driverId) {
		this.driverId = driverId;
	}

	public String getDriveryLicenseNo() {
		return driveryLicenseNo;
	}

	public void setDriveryLicenseNo(String driveryLicenseNo) {
		this.driveryLicenseNo = driveryLicenseNo;
	}

	public String getMaxTransportCapacity() {
		return maxTransportCapacity;
	}

	public void setMaxTransportCapacity(String maxTransportCapacity) {
		this.maxTransportCapacity = maxTransportCapacity;
	}

	public String getMaterial() {
		return material;
	}

	public void setMaterial(String material) {
		this.material = material;
	}

	public long getUserId() {
		return userId;
	}

	public void setUserId(long userId) {
		this.userId = userId;
	}
	
	

}
