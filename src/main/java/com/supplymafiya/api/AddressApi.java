package com.supplymafiya.api;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.supplymafiya.model.Block;
import com.supplymafiya.model.District;
import com.supplymafiya.model.ErrorResponse;
import com.supplymafiya.model.State;
import com.supplymafiya.service.AddressService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2018-02-28T09:43:00.518+05:30")

@RestController
@RequestMapping("/seller")
@CrossOrigin(maxAge = 3600, allowedHeaders = { "Content-Type", "Accept" }, allowCredentials = "false", methods = {
		RequestMethod.GET, RequestMethod.POST, RequestMethod.PATCH, RequestMethod.DELETE }, origins = { "*" })
@Api(value = "get Address state, district, block")
public class AddressApi {

	@Autowired
	AddressService addressService;

	@ApiOperation(value = "get state list", tags = "location")
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Success", response = State.class ,responseContainer = "List"),
			@ApiResponse(code = 400, message = "Bad request", response = ErrorResponse.class),
			@ApiResponse(code = 401, message = "Unauthorized", response = ErrorResponse.class),
			@ApiResponse(code = 404, message = "Not found", response = ErrorResponse.class),
			@ApiResponse(code = 405, message = "Validation exception", response = ErrorResponse.class) })
	@GetMapping(value = "/state", produces = { "application/json" })
	public ResponseEntity<List<State>> getStateList(
			//@ApiParam(value = "User ID", required = true) @RequestHeader(value = "USER_ID", required = true) long USER_ID
			)
			throws Exception {
		List<State> response = new ArrayList<>();
		response = addressService.get();
		return new ResponseEntity<List<State>>(response, HttpStatus.OK);
	}

	@ApiOperation(value = "get district list", tags = "location")
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Success", response = District.class, responseContainer = "List"),
			@ApiResponse(code = 400, message = "Bad request", response = ErrorResponse.class),
			@ApiResponse(code = 401, message = "Unauthorized", response = ErrorResponse.class),
			@ApiResponse(code = 404, message = "Not found", response = ErrorResponse.class),
			@ApiResponse(code = 405, message = "Validation exception", response = ErrorResponse.class) })
	@GetMapping("/district/{state_id}")
	public List<District> getdistrictList(
			//@ApiParam(value = "User ID", required = true) @RequestHeader(value = "USER_ID", required = true) long USER_ID,
			@ApiParam(value = "State Id", required = true) @PathVariable("state_id") long stateId) throws Exception {
		List<District> list = new ArrayList<District>();
		list = addressService.getDistrictList(stateId);
		return list;
	}

	@ApiOperation(value = "get block list", tags = "location")
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Success", response = Block.class, responseContainer = "List"),
			@ApiResponse(code = 400, message = "Bad request", response = ErrorResponse.class),
			@ApiResponse(code = 401, message = "Unauthorized", response = ErrorResponse.class),
			@ApiResponse(code = 404, message = "Not found", response = ErrorResponse.class),
			@ApiResponse(code = 405, message = "Validation exception", response = ErrorResponse.class) })
	@GetMapping("/block/{district_id}")
	public List<Block> getBlockList(
			//@ApiParam(value = "User ID", required = true) @RequestParam(value = "USER_ID", required = true) long USER_ID,
			@ApiParam(value = "District Id", required = true) @PathVariable("district_id") long districtId)
			throws Exception {
		List<Block> list = new ArrayList<Block>();
		list = addressService.getBlockList(districtId);
		return list;
	}

}
