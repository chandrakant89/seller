package com.supplymafiya.api;

import org.json.JSONException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.mashape.unirest.http.JsonNode;
import com.mashape.unirest.http.exceptions.UnirestException;
import com.supplymafiya.model.ErrorResponse;
import com.supplymafiya.model.Otp;
import com.supplymafiya.otp.IOTPService;
import com.supplymafiya.otp.OTPServiceProviderFactory;
import com.supplymafiya.response.ResponseMessageDTO;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@RestController
@RequestMapping("/seller")
@CrossOrigin(maxAge = 3600, allowedHeaders= {"Content-Type","Accept"}, allowCredentials="false",methods= {RequestMethod.GET, RequestMethod.POST, RequestMethod.PATCH, RequestMethod.DELETE}, origins= {"*"})
@Api(value = "otp api")
public class OTPVerificationApi {
	
	
	
	
	@ApiOperation(value = "send otp" , tags = "otpverification")
    @ApiResponses(value = {
    		@ApiResponse(code = 200, message = "Success"),
			@ApiResponse(code = 400, message = "Bad request", response = ErrorResponse.class),
			@ApiResponse(code = 401, message = "Unauthorized", response = ErrorResponse.class),
			@ApiResponse(code = 404, message = "Not found", response = ErrorResponse.class),
			@ApiResponse(code = 405, message = "Validation exception", response = ErrorResponse.class)})
	@RequestMapping(value="/sendotpmessage/{mobile_number}",
	produces = { "application/json" }, 
	method = RequestMethod.GET)
    public ResponseEntity<ResponseMessageDTO> sendOTPMessage(
    		@ApiParam(value = "Mobile Number", required = true) @PathVariable("mobile_number") String mobileNumber) throws Exception {
		IOTPService iotpService=OTPServiceProviderFactory.getOTPService("SMS91");
		JsonNode body = iotpService.sendOTPMessage(mobileNumber);
		ResponseMessageDTO responseMessageDTO = new ResponseMessageDTO();
		responseMessageDTO.setType(body.getObject().getString("type"));
		responseMessageDTO.setMessage(body.getObject().getString("message"));
		return new ResponseEntity<ResponseMessageDTO>(responseMessageDTO,HttpStatus.OK);
    }
	
	@ApiOperation(value = "otp verification" , tags = "otpverification")
    @ApiResponses(value = {
    		@ApiResponse(code = 200, message = "Success"),
			@ApiResponse(code = 400, message = "Bad request", response = ErrorResponse.class),
			@ApiResponse(code = 401, message = "Unauthorized", response = ErrorResponse.class),
			@ApiResponse(code = 404, message = "Not found", response = ErrorResponse.class),
			@ApiResponse(code = 405, message = "Validation exception", response = ErrorResponse.class)})
	@PostMapping("/verifyotp/{mobile_number}")
	public ResponseEntity<ResponseMessageDTO> verifyOTP(
			@ApiParam(value = "Mobile Number", required = true) @PathVariable("mobile_number") String mobileNumber,
			@RequestBody Otp otp) throws UnirestException, JSONException {
		IOTPService iotpService=OTPServiceProviderFactory.getOTPService("SMS91");
		JsonNode body =iotpService.verifyOTP(mobileNumber,otp.getOtp());
		ResponseMessageDTO responseMessageDTO = new ResponseMessageDTO();
		if(body.getObject().getString("type") == "success") {
			responseMessageDTO.setType(body.getObject().getString("type"));
			responseMessageDTO.setMessage(body.getObject().getString("message"));
		}else {
			responseMessageDTO.setType(body.getObject().getString("type"));
			responseMessageDTO.setMessage(body.getObject().getString("message"));
		}
		return new ResponseEntity<ResponseMessageDTO>(responseMessageDTO, HttpStatus.OK);
	}
	
	@ApiOperation(value = "resend otp" , tags = "otpverification")
    @ApiResponses(value = {
    		@ApiResponse(code = 200, message = "Success"),
			@ApiResponse(code = 400, message = "Bad request", response = ErrorResponse.class),
			@ApiResponse(code = 401, message = "Unauthorized", response = ErrorResponse.class),
			@ApiResponse(code = 404, message = "Not found", response = ErrorResponse.class),
			@ApiResponse(code = 405, message = "Validation exception", response = ErrorResponse.class)})
	@RequestMapping(value="/resendotpmessage/{mobile_number}",
	produces = { "application/json" }, 
	method = RequestMethod.GET)
	public ResponseEntity<ResponseMessageDTO> resendOTPMessage(
			@ApiParam(value = "Mobile Number", required = true)
			@PathVariable("mobile_number") String mobileNumber) throws UnirestException, JSONException {
		IOTPService iotpService=OTPServiceProviderFactory.getOTPService("SMS91");
		JsonNode body =iotpService.resendOTPMessage(mobileNumber);
		ResponseMessageDTO responseMessageDTO = new ResponseMessageDTO();
		responseMessageDTO.setType(body.getObject().getString("type"));
		responseMessageDTO.setMessage(body.getObject().getString("message"));
		return new ResponseEntity<ResponseMessageDTO>(responseMessageDTO,HttpStatus.OK);
	}

}
