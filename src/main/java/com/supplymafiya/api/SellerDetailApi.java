package com.supplymafiya.api;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.supplymafiya.exception.LoginMobNoRegisteredException;
import com.supplymafiya.exception.NotFoundException;
import com.supplymafiya.model.ErrorResponse;
import com.supplymafiya.model.Product;
import com.supplymafiya.model.Roles;
import com.supplymafiya.model.SellerDetail;
import com.supplymafiya.response.ResponseMessageDTO;
import com.supplymafiya.service.SellerService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2018-02-28T09:43:00.518+05:30")

@RestController
@RequestMapping("/seller")
@CrossOrigin(maxAge = 3600, allowedHeaders = { "Content-Type", "Accept"}, allowCredentials = "false", methods = { RequestMethod.GET,
				RequestMethod.POST, RequestMethod.PATCH, RequestMethod.DELETE }, origins = { "*" })
@Api(value = "add seller")
public class SellerDetailApi {

	@Autowired
	SellerService sellerService;

	@ApiOperation(value = "add seller", tags = "seller")
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Success"),
			@ApiResponse(code = 400, message = "Bad request", response = ErrorResponse.class),
			@ApiResponse(code = 401, message = "Unauthorized", response = ErrorResponse.class),
			@ApiResponse(code = 404, message = "Not found", response = ErrorResponse.class),
			@ApiResponse(code = 405, message = "Validation exception", response = ErrorResponse.class) })
	@PostMapping(value = "/addSeller", produces = { "application/json" }, consumes = { "application/json" })
	public ResponseEntity<ResponseMessageDTO> addSeller(
			@RequestBody SellerDetail seller) throws Exception {
		long data = sellerService.addSeller(seller);
		ResponseMessageDTO response = new ResponseMessageDTO();
		response.setMessage("Seller Registered Successfully");
		return new ResponseEntity<ResponseMessageDTO>(response, HttpStatus.CREATED);
	}

	@ApiOperation(value = "get User list", tags = "seller", response = SellerDetail.class, responseContainer = "List")
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Success", response = SellerDetail.class),
			@ApiResponse(code = 400, message = "Bad request", response = ErrorResponse.class),
			@ApiResponse(code = 401, message = "Unauthorized", response = ErrorResponse.class),
			@ApiResponse(code = 404, message = "Not found", response = ErrorResponse.class),
			@ApiResponse(code = 405, message = "Validation exception", response = ErrorResponse.class) })
	@RequestMapping(value = "/getuserlist/{added_by}", produces = { "application/json" }, method = RequestMethod.GET)
	public ResponseEntity<List<SellerDetail>> getUserList(
			@ApiParam(value = "User ID" ,required=true) @PathVariable("added_by") long addedBy) throws Exception {
		List<SellerDetail> response = sellerService.getUserList(addedBy);
		return new ResponseEntity<List<SellerDetail>>(response, HttpStatus.OK);
	}

	@ApiOperation(value = "get role", tags = "seller")
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Success", response = Roles.class, responseContainer = "List"),
			@ApiResponse(code = 400, message = "Bad request", response = ErrorResponse.class),
			@ApiResponse(code = 401, message = "Unauthorized", response = ErrorResponse.class),
			@ApiResponse(code = 404, message = "Not found", response = ErrorResponse.class),
			@ApiResponse(code = 405, message = "Validation exception", response = ErrorResponse.class) })
	@GetMapping(value = "/roles/{roleId}", produces = "application/json")
	public Roles getRoles(
		@PathVariable("roleId") long roleId) throws Exception {
		Roles response = new Roles();
		response = sellerService.getRoles(roleId);
		return response;
	}

	@ApiOperation(value = "get Seller By Id", tags = "seller")
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Success", response = SellerDetail.class),
			@ApiResponse(code = 400, message = "Bad request", response = ErrorResponse.class),
			@ApiResponse(code = 401, message = "Unauthorized", response = ErrorResponse.class),
			@ApiResponse(code = 404, message = "Not found", response = ErrorResponse.class),
			@ApiResponse(code = 405, message = "Validation exception", response = ErrorResponse.class) })
	@GetMapping(value = "/getuserbyid/{user_id}", produces = "application/json")
	public SellerDetail getUserById(@PathVariable("user_id") long userId) throws Exception {
		SellerDetail response = new SellerDetail();
		response = sellerService.getUserById(userId);

		System.out.println("data is" + response.getOwnerMobile());
		return response;
	}

	@ApiOperation(value = "get Seller By mobile no", tags = "seller")
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Success", response = SellerDetail.class),
			@ApiResponse(code = 400, message = "Bad request", response = ErrorResponse.class),
			@ApiResponse(code = 401, message = "Unauthorized", response = ErrorResponse.class),
			@ApiResponse(code = 404, message = "Not found", response = ErrorResponse.class),
			@ApiResponse(code = 405, message = "Validation exception", response = ErrorResponse.class) })
	@GetMapping(value = "/getUserbymobileno/{owner_mobile}", produces = "application/json")
	public SellerDetail getUserByOwnerMobileNo(@PathVariable("owner_mobile") String ownerMobile) throws Exception {
		SellerDetail response = new SellerDetail();
		response = sellerService.getUserByOwnerMobileNo(ownerMobile);
		System.out.println("data is: " + response.getOwnerMobile());
		return response;
	}
	
	@ApiOperation(value = "get Seller By mobile no", tags = "seller")
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Success"),
			@ApiResponse(code = 400, message = "Bad request", response = ErrorResponse.class),
			@ApiResponse(code = 401, message = "Unauthorized", response = ErrorResponse.class),
			@ApiResponse(code = 404, message = "Not found", response = ErrorResponse.class),
			@ApiResponse(code = 405, message = "Validation exception", response = ErrorResponse.class) })
	@GetMapping(value = "/isexit/{owner_mobile}/{owner_email}", produces = "application/json")
	public ResponseMessageDTO isExit(@PathVariable("owner_mobile") String ownerMobile, @PathVariable("owner_email") String ownerEmail) throws Exception {
		
		boolean data = sellerService.isExits(ownerMobile, ownerEmail);
		if(data) {
			throw new LoginMobNoRegisteredException("User Already Exit");
		}
		ResponseMessageDTO responseMessageDTO = new ResponseMessageDTO();
		responseMessageDTO.setMessage("Not Exit, User can be created with these Credential");
		return responseMessageDTO;
		
	}

}
