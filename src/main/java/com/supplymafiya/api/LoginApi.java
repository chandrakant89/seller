package com.supplymafiya.api;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.supplymafiya.exception.BadRequestException;
import com.supplymafiya.exception.UnauthorizedException;
import com.supplymafiya.model.ChangePassword;
import com.supplymafiya.model.ErrorResponse;
import com.supplymafiya.model.LoginRequest;
import com.supplymafiya.model.SellerDetail;
import com.supplymafiya.response.LoginResponseDTO;
import com.supplymafiya.response.ResponseMessageDTO;
import com.supplymafiya.service.LoginService;
import com.supplymafiya.service.SellerService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@RestController
@RequestMapping("/seller")
@CrossOrigin(maxAge = 3600, allowedHeaders = { "Content-Type", "Accept" }, allowCredentials = "false", methods = {
		RequestMethod.GET, RequestMethod.POST, RequestMethod.PATCH, RequestMethod.DELETE }, origins = { "*" })
@Api(value = "login")
public class LoginApi {

	@Autowired
	LoginService loginService;

	@Autowired
	SellerService sellerService;

	@ApiOperation(value = "login", tags = "login", response = LoginResponseDTO.class)
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Success", response = LoginResponseDTO.class),
			@ApiResponse(code = 400, message = "Bad request", response = ErrorResponse.class),
			@ApiResponse(code = 405, message = "Validation exception", response = ErrorResponse.class) })
	@PostMapping(value = "/login")
	public ResponseEntity<LoginResponseDTO> login(
			@ApiParam(value = "Request for Login", required = true) @Valid @RequestBody LoginRequest body)
			throws Exception {
		LoginResponseDTO loginResponseDTO = new LoginResponseDTO();
		boolean isValidUser = loginService.validateUser(body);
		if (!isValidUser) {
			throw new UnauthorizedException("Wrong Credential");
			// loginResponseDTO.setMessage("Wrong Credential");
		} else {

			String ownerMobile = body.getMobile();
			SellerDetail sellerDetail = sellerService.getUserByOwnerMobileNo(ownerMobile);
			loginResponseDTO.setMessage("Success");
			loginResponseDTO.setUserId(sellerDetail.getUserId());

			loginResponseDTO.setUserRole(sellerDetail.getUserRole());
			loginResponseDTO.setOwnerName(sellerDetail.getOwnerName());
			loginResponseDTO.setOwnerEmail(sellerDetail.getOwnerEmail());
			// loginResponseDTO.setPassword(sellerDetail.getPassword());
			loginResponseDTO.setOwnerMobile(sellerDetail.getOwnerMobile());
			loginResponseDTO.setOwnerWhatsappNo(sellerDetail.getOwnerWhatsappNo());
			loginResponseDTO.setGender(sellerDetail.getGender());
			loginResponseDTO.setState(sellerDetail.getState());
			loginResponseDTO.setDistrict(sellerDetail.getDistrict());
			loginResponseDTO.setBlock(sellerDetail.getBlock());
			loginResponseDTO.setRepresentativeName(sellerDetail.getRepresentativeName());
			loginResponseDTO.setRepresentativeContactNo(sellerDetail.getRepresentativeContactNo());
			loginResponseDTO.setRepresentativeEmailId(sellerDetail.getRepresentativeEmailId());
			loginResponseDTO.setAadharCard(sellerDetail.getAadharCard());
			loginResponseDTO.setPanCard(sellerDetail.getPanCard());
			loginResponseDTO.setBussinesName(sellerDetail.getBussinesName());
			loginResponseDTO.setGstn(sellerDetail.getGstn());
			loginResponseDTO.setAccountNo(sellerDetail.getAccountNo());
			loginResponseDTO.setAccountHolderName(sellerDetail.getAccountHolderName());
			loginResponseDTO.setBranch(sellerDetail.getBranch());
			loginResponseDTO.setIfscCode(sellerDetail.getIfscCode());
			loginResponseDTO.setDate(sellerDetail.getDate());
			loginResponseDTO.setAddedBy(sellerDetail.getAddedBy());
			loginResponseDTO.setVerifyStatus(sellerDetail.getVerifyStatus());

		}
		return new ResponseEntity<LoginResponseDTO>(loginResponseDTO, HttpStatus.OK);
	}

	@ApiOperation(value = "changepassword", tags = "login")
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Success"),
			@ApiResponse(code = 400, message = "Bad request", response = ErrorResponse.class),
			@ApiResponse(code = 405, message = "Validation exception", response = ErrorResponse.class) })
	@PatchMapping(value = "/changepassword/{user_id}")
	public ResponseEntity<String> changePassword(
			@ApiParam(value = "Request for change password", required = true) @Valid @PathVariable(value = "user_id") long userId,
			@RequestBody ChangePassword changePassword) throws Exception {
		SellerDetail response = sellerService.getUserById(userId);
		System.out.println("Old Password: " + response.getPassword());// vishnukant

		if (response.getPassword().equals(changePassword.getOldpassword())) {
			if (changePassword.getNewpassword().equals(changePassword.getConfirmpassword())) {
				String pass = changePassword.getNewpassword();
				System.out.println("New Password: " + pass);
				@SuppressWarnings("unused")
				long data = sellerService.addNewPassword(pass, userId);
			} else {
				throw new UnauthorizedException("Wrong Credential");
			}
		} else {
			throw new UnauthorizedException("Wrong Credential");
		}

		return new ResponseEntity<String>("success", HttpStatus.OK);
	}

	@ApiOperation(value = "forgotpassword", tags = "login")
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Success"),
			@ApiResponse(code = 400, message = "Bad request", response = ErrorResponse.class),
			@ApiResponse(code = 405, message = "Validation exception", response = ErrorResponse.class) })
	@PatchMapping(value = "/forgotpassword/mobile/{mobile_no}/otpStatus/{otp_Status}")
	public ResponseEntity<ResponseMessageDTO> forgotPassword(
			@ApiParam(value = "Request for change password", required = true) @Valid @PathVariable(value = "mobile_no") String mobileNo,
			@Valid @PathVariable(value = "otp_Status") boolean otpStatus, @RequestBody ChangePassword changePassword)
			throws Exception {
		SellerDetail response = loginService.isMobileNoexist(mobileNo);
		if (response.getOwnerMobile().equals(mobileNo)) {
			String pass = changePassword.getNewpassword();
			System.out.println("New Password: " + pass);
			@SuppressWarnings("unused")
			long data = sellerService.addforgotPassword(pass, mobileNo, otpStatus);
		} else {
			throw new BadRequestException("Wrong Mobile");
		}
		ResponseMessageDTO responseMessageDTO = new ResponseMessageDTO();
		responseMessageDTO.setMessage("Your Password Changed Successfully. Please Visit home page to login");
		return new ResponseEntity<ResponseMessageDTO>(responseMessageDTO, HttpStatus.OK);

	}

	@ApiOperation(value = "isMobileNoexist", tags = "login")
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Success", response = SellerDetail.class),
			@ApiResponse(code = 400, message = "Bad request", response = ErrorResponse.class),
			@ApiResponse(code = 405, message = "Validation exception", response = ErrorResponse.class) })
	@GetMapping(value = "/isMobileNoexist/{mobile_no}")
	public ResponseEntity<SellerDetail> isMobileNoexist(
			@ApiParam(value = "Check is Mobile No exist", required = true) @Valid @PathVariable(value = "mobile_no") String mobile_no)
			throws Exception {
		SellerDetail response = loginService.isMobileNoexist(mobile_no);
		return new ResponseEntity<SellerDetail>(response, HttpStatus.OK);
	}

	/*
	 * private boolean isRememberMeAuthenticated() {
	 * 
	 * Authentication authentication =
	 * SecurityContextHolder.getContext().getAuthentication(); if (authentication ==
	 * null) { return false; }
	 * 
	 * return
	 * RememberMeAuthenticationToken.class.isAssignableFrom(authentication.getClass(
	 * )); }
	 * 
	 *//**
		 * save targetURL in session
		 */
	/*
	 * private void setRememberMeTargetUrlToSession(HttpServletRequest request){
	 * HttpSession session = request.getSession(false); if(session!=null){
	 * session.setAttribute("targetUrl", "/seller/addSeller"); } }
	 * 
	 *//**
		 * get targetURL from session
		 *//*
			 * private String getRememberMeTargetUrlFromSession(HttpServletRequest request){
			 * String targetUrl = ""; HttpSession session = request.getSession(false);
			 * if(session!=null){ targetUrl = session.getAttribute("targetUrl")==null?""
			 * :session.getAttribute("targetUrl").toString(); } return targetUrl; }
			 */
}
