package com.supplymafiya.api;

import java.util.List;

import javax.servlet.ServletContext;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.ServletContextAware;

import com.supplymafiya.model.ErrorResponse;
import com.supplymafiya.model.Product;
import com.supplymafiya.response.ResponseMessageDTO;
import com.supplymafiya.service.ProductService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@RestController
@RequestMapping("/seller")
@CrossOrigin(maxAge = 3600, allowedHeaders= {"Content-Type","Accept"}, allowCredentials="false",methods= {RequestMethod.GET, RequestMethod.POST, RequestMethod.PATCH, RequestMethod.DELETE}, origins= {"*"})
@Api(value = "add product")
public class ProductDetailApi{
	

	@Autowired
	ProductService productService;
	
	
	@ApiOperation(value = "add Product" , tags = "product")
	 @ApiResponses(value = {
	    		@ApiResponse(code = 200, message = "Success"),
				@ApiResponse(code = 400, message = "Bad request", response = ErrorResponse.class),
				@ApiResponse(code = 401, message = "Unauthorized", response = ErrorResponse.class),
				@ApiResponse(code = 404, message = "Not found", response = ErrorResponse.class),
				@ApiResponse(code = 405, message = "Validation exception", response = ErrorResponse.class)})


	@PostMapping(value="/addProduct/{user_id}", produces = { "application/json" },consumes = { "application/json" })
			public ResponseMessageDTO addProduct(
			@ApiParam(value = "User ID" ,required=true)  @PathVariable("user_id") long USER_ID,
			@RequestBody Product product) throws Exception {
     ResponseMessageDTO responseMessageDTO = new ResponseMessageDTO();
     productService.addProduct(product, USER_ID);
     responseMessageDTO.setMessage("Product added Successfully");
      return responseMessageDTO;
    }
	
	@ApiOperation(value = "get product list" , tags = "product", response = Product.class, responseContainer = "List")
    @ApiResponses(value = {
    		@ApiResponse(code = 200, message = "Success", response = Product.class),
			@ApiResponse(code = 400, message = "Bad request", response = ErrorResponse.class),
			@ApiResponse(code = 401, message = "Unauthorized", response = ErrorResponse.class),
			@ApiResponse(code = 404, message = "Not found", response = ErrorResponse.class),
			@ApiResponse(code = 405, message = "Validation exception", response = ErrorResponse.class)})
	@RequestMapping(value="/getProductList/{user_id}",
	produces = { "application/json" }, 
	method = RequestMethod.GET)
    public ResponseEntity<List<Product>> getProductList(
    		@ApiParam(value = "User ID" ,required=true)  @PathVariable("user_id") long USER_ID) throws Exception {
        List<Product> response = productService.getProductList(USER_ID);
        return new ResponseEntity<List<Product>> (response, HttpStatus.OK);
    }
	
	@ApiOperation(value = "get product By Id" , tags = "product")
    @ApiResponses(value = {
    		@ApiResponse(code = 200, message = "Success"),
			@ApiResponse(code = 400, message = "Bad request", response = ErrorResponse.class),
			@ApiResponse(code = 401, message = "Unauthorized", response = ErrorResponse.class),
			@ApiResponse(code = 404, message = "Not found", response = ErrorResponse.class),
			@ApiResponse(code = 405, message = "Validation exception", response = ErrorResponse.class)})
	//@GetMapping(value="/getproductbyid/{productid}", produces= {"application/json"})
	@RequestMapping(value = "/getProduct/{productid}",
	produces = { "application/json" }, 
	method = RequestMethod.GET)
    public ResponseEntity<Product> getProductById(@PathVariable("productid") long productId) throws Exception {
     Product response = productService.getProductById(productId);
        return new ResponseEntity<Product> (response, HttpStatus.OK);
    }

}
