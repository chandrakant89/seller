/*package com.supplymafiya.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.web.authentication.SavedRequestAwareAuthenticationSuccessHandler;

@EnableWebSecurity
public class SecurityConfig extends  WebSecurityConfigurerAdapter{
	
	private static final String USER = "USER";
	private static final String ADMIN = "ADMIN";

	public void configure(HttpSecurity httpSecurity) throws Exception {
		httpSecurity.authorizeRequests()
		.antMatchers("/admin/**").hasRole(ADMIN)
		.and()
		.formLogin()
		.successHandler(savedRequestAwareAuthenticationSuccessHandler())
		.loginPage("/seller/login")
		.failureUrl("/login?error");
		
	}
	
	@Autowired
	public void configureGlobal(AuthenticationManagerBuilder auth) throws Exception {
		auth.inMemoryAuthentication()
			.withUser("chandra").password("chandra").roles(USER)
		.and()
			.withUser("vishnu").password("vishnu").roles(ADMIN);
	}
	
	
	@Autowired
	public SavedRequestAwareAuthenticationSuccessHandler 
                savedRequestAwareAuthenticationSuccessHandler() {
		
               SavedRequestAwareAuthenticationSuccessHandler auth 
                    = new SavedRequestAwareAuthenticationSuccessHandler();
		auth.setTargetUrlParameter("targetUrl");
		return auth;
	}	
}
*/