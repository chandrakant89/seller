package com.supplymafiya.utility;

import java.util.Random;

public class SKUGenerater {
	
	public static String generateSKU(){
		String sku = "NHSLPD";
		final Random rn = new Random();
		final String alphabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
		int number = 0;

		for (int i = 0; i < 3; i++) {
			int index = rn.nextInt() % 26;
			if (index < 0) {
				index = 0;
			}
			number = rn.nextInt() % 10;
			//sku += alphabet.substring(index, index + 1) + (number < 0 ? -1 * number : number);
			sku += (number < 0 ? -1 * number : number)+alphabet.substring(index,index + 1);
		}
		return sku;
	}
	

}
