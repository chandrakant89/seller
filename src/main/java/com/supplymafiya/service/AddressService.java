package com.supplymafiya.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.supplymafiya.model.Block;
import com.supplymafiya.model.District;
import com.supplymafiya.model.State;
import com.supplymafiya.repository.AddressRepository;

@Service
public class AddressService {
	
	@Autowired
	AddressRepository addressRepository;

	public List<State> get() throws Exception {
		List<State> data = new ArrayList<>();
		data = addressRepository.get();
		return data;
	}
	
	public List<District> getDistrictList(long stateId) throws Exception {
		List<District> data = new ArrayList<>();
		data = addressRepository.getDistrictList(stateId);
		return data;
	}
	
	public List<Block> getBlockList(long districtId) throws Exception {
		List<Block> data = new ArrayList<Block>();
		data = addressRepository.getBlockList(districtId);
		return data;
	}
}
