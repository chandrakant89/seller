package com.supplymafiya.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.supplymafiya.model.LoginRequest;
import com.supplymafiya.model.SellerDetail;
import com.supplymafiya.repository.SellerDetailRepository;
import com.supplymafiya.utility.EncryptDecriptPassword;

@Service
public class LoginService {
	
	@Autowired
	SellerDetailRepository sellerDetailRepository;

	public boolean validateUser(LoginRequest body) throws Exception {
		
		boolean flag = false;
		String mobile = body.getMobile();
		String password = body.getPassword();
		int offset = 5;
		
		SellerDetail sellerDetail = sellerDetailRepository.getUserByOwnerMobileNo(mobile);
		String newpassword = sellerDetail.getPassword();
		String decriptedPass = EncryptDecriptPassword.decrypt(newpassword, offset);
		/*System.out.println("Decripted Password: "+decriptedPass);*/
		if (sellerDetail.getOwnerMobile().equals(mobile) && decriptedPass.equals(password)) {
			flag = true;
		}else {
			flag = false;
		}
		return flag;
		
	}

	public SellerDetail isMobileNoexist(String mobile_no) {
		SellerDetail sellerDetail = sellerDetailRepository.getUserByOwnerMobileNo(mobile_no);
		SellerDetail sellerMobile =  new SellerDetail();
		if(sellerDetail != null) {
			sellerMobile.setOwnerMobile(mobile_no);
		}else {
			sellerMobile = null;
		}
		return sellerMobile;
	}

	
	

}
