package com.supplymafiya.service;

import java.util.List; 

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.supplymafiya.exception.LoginMobNoRegisteredException;
import com.supplymafiya.model.Roles;
import com.supplymafiya.model.SellerDetail;
import com.supplymafiya.repository.SellerDetailRepository;
import com.supplymafiya.utility.EncryptDecriptPassword;

@Service
public class SellerService {
	
	@Autowired
	SellerDetailRepository sellerDetailRepository; 
	
	public long addSeller(SellerDetail seller) throws Exception {
		long data = sellerDetailRepository.addSeller(seller);
		return data;
	}
	
	public Roles getRoles(long roleId){
		Roles data = new Roles();
		data = sellerDetailRepository.getRoles(roleId);
		return data;
	}

	public SellerDetail getUserById(long userId) {
		SellerDetail data = new SellerDetail();
		data = sellerDetailRepository.getUserById(userId);
		String pass = data.getPassword();
		System.out.println("sellerService return password: "+pass);// vishnukant
			return data;
	}

	public SellerDetail getUserByOwnerMobileNo(String ownerMobile) {
		SellerDetail data = new SellerDetail();
		int offset = 5;
		data = sellerDetailRepository.getUserByOwnerMobileNo(ownerMobile);
		String ecrpass = data.getPassword();
		String dcrpass = EncryptDecriptPassword.decrypt(ecrpass, offset);
		data.setPassword(dcrpass);
		return data;
	}

	public List<SellerDetail> getUserList(long addedBy) {
		List<SellerDetail> userList = sellerDetailRepository.getUserList(addedBy);
		return userList;
	}

	public boolean isExits(String ownerMobile, String ownerEmail) {
		boolean exit = sellerDetailRepository.isExits(ownerMobile, ownerEmail);
		if(exit) {
			return true;
		}
		return false;
	}

	public long addNewPassword(String pass, long userId) {
		long data = sellerDetailRepository.addNewPassword(pass, userId);
		
		return data;
		
	}

	public SellerDetail getUserPassword(String mobile, long userId) {
		SellerDetail data = new SellerDetail();
		 data = sellerDetailRepository.getUserPassword(mobile,userId);
		return data;
	}

	public long addforgotPassword(String pass, String mobileNo, boolean otpStatus) {
		Long data= null;
		if(otpStatus) {
			data = sellerDetailRepository.addforgotPassword(pass, mobileNo);
		}
		return data;
	}

}
