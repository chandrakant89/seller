package com.supplymafiya.service;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.supplymafiya.exception.BadRequestException;
import com.supplymafiya.exception.FileStorageException;
import com.supplymafiya.model.FileStorageProperties;
import com.supplymafiya.model.ImageDetail;
import com.supplymafiya.repository.FileUploadRepository;


@Service
public class FileUploadService {
	
	@Autowired
	FileUploadRepository fileUploadRepository;
	
	private final Path fileStorageLocation;
	
	@Autowired
	 public FileUploadService(FileStorageProperties fileStorageProperties) {
	        this.fileStorageLocation = Paths.get(fileStorageProperties.getUploadDir());
	                /*.toAbsolutePath().normalize();*/
	        try {
	            Files.createDirectories(this.fileStorageLocation);
	        } catch (Exception ex) {
	            throw new FileStorageException("Could not create the directory where the uploaded files will be stored.", ex);
	        }
	    }
	

	public String uploadImage(MultipartFile imageFile) throws Exception {
		String fileUrl = uploadFile(imageFile);
		System.out.println("File Url: "+fileUrl);
		return fileUrl;
	}

	/*public String uploadVideo(String userToken, String cityId, String userIp, String userDevice, String sessionId,
			String moduleType, MultipartFile vidoeFile) throws Exception {
		logger.info("Validate user-token " + userToken + " for city " + cityId);
		umsService.validateApiUserResourceAccess(userToken, sessionId, cityId, "videos", "POST");

		logger.info("Getting city-code");
		String cityCode = CityContext.getCurrentCity().getCityCode();
		logger.info("Got city-code -> " + cityCode);

		logger.info("Uploading video: size:" + vidoeFile.getSize());
		String fileUrl = uploadFile(cityCode, moduleType, vidoeFile);
		logger.info("Uploaded video at " + fileUrl);
		return fileUrl;
	}*/

	private String uploadFile(MultipartFile multipartFile) throws IOException {
		if (multipartFile == null || multipartFile.isEmpty()) {
			throw new BadRequestException("Empty file");
		}
		File file = convertMultiPartToFile(multipartFile);
		String fileName = generateFileName(multipartFile);
		Path targetLocation = this.fileStorageLocation;
		System.out.println("File name: "+fileName);
		
		String fileUrl = targetLocation+"\\"+fileName;
		//logger.info("Uploading file " + multipartFile.getOriginalFilename() + " to " + fileUrl);
		uploadFileTos3bucket(fileName, multipartFile);
		file.delete();
		return fileUrl;
	}

	private File convertMultiPartToFile(MultipartFile file) throws IOException {
		File convFile = new File(file.getOriginalFilename());
		FileOutputStream fos = new FileOutputStream(convFile);
		fos.write(file.getBytes());
		fos.close();
		return convFile;
	}

	private String generateFileName(MultipartFile multiPart) {
		return new Date().getTime() + "-" + multiPart.getOriginalFilename().replace(" ", "_");
	}

	private void uploadFileTos3bucket(String fileName, MultipartFile file) throws IOException {
		Path targetLocation = this.fileStorageLocation.resolve(fileName);
        System.out.println("Location: "+targetLocation);
      //Files.copy(file.getInputStream(), targetLocation, StandardCopyOption.REPLACE_EXISTING);
        Files.copy((file).getInputStream(), targetLocation);
	}


	public long addImageDetail(String url) {
		long data = fileUploadRepository.addImageDetail( url);
		return data;
	}


}
