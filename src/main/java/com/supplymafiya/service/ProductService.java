
package com.supplymafiya.service;

import java.util.List; 

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.supplymafiya.model.Product;
import com.supplymafiya.repository.ProductRepository;

@Service
public class ProductService {
	
	@Autowired
	ProductRepository productRepository; 
	
	public long addProduct(Product product,long USER_ID) throws Exception {
		long data = productRepository.addProduct(product, USER_ID);
		return data;
	}

	public List<Product> getProductList(long USER_ID) {
		List<Product> productList = productRepository.getProductList(USER_ID);
		return productList;
	}

	public Product getProductById(long productId) {
		Product data = new Product(); 
		data = productRepository.getProductById(productId);
		return data;
	}

}