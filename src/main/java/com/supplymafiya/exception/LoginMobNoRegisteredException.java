package com.supplymafiya.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.BAD_REQUEST)
public class LoginMobNoRegisteredException extends RuntimeException {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 371219886726539232L;

	public LoginMobNoRegisteredException(String exception) {
		super(exception);
	}

}
